import json
import requests
import sys
import pyfiglet

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

word = str(sys.argv[1])

response = requests.get("http://api.urbandictionary.com/v0/define?term="+word)
parsed_json = json.loads(response.text)

print(bcolors.HEADER + pyfiglet.figlet_format(word) + bcolors.ENDC)
print(bcolors.OKGREEN + parsed_json['list'][0]['definition'] + bcolors.ENDC)

