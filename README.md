# Urban Dictionary API 

I used the public facing Urban Dictionary API to wrap my mind around python requests and json library

Just pass any word as an argument and behold the definition of that word according to Urban Dictionary's highest ranked entry for that word

### Example

```willemdauwen$ python3 urban.py Willem```